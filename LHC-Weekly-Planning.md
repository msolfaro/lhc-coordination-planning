# LHC machine coordination planning

## Coordinators (24.5 - 2.6): J. Wenninger (164367), M. Solfaroli (deputy)

## Week 20 planning

* Intensity ramp up to 75b
* Crystal collimation test (Wed afternoon)


### Max intensities:
* Injection : 2800 bunches of 1.2E11 ppb
* 6.8 teV : 75b of 1.2E11 ppb
